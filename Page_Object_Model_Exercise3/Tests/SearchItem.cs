﻿using NUnit.Framework;

namespace Page_Object_Model_Exercise3.Tests
{
    [TestFixture]
    public class SearchItem : Base
    {
        [Test]
        public void GivenAnExistingItemName_WhenSearching_ThenReturnItemCountGreaterThanZero()
        {
            StoreApp.SearchPage.Search();
            StoreApp.SearchPage.WaitForResults(); 

            Assert.IsTrue(StoreApp.SearchPage.SearchedItemNames.Count > 0);
        }

        [Test]
        public void GivenLinkButton_WhenRedirecting_ThenConfirmTheNewURL()
        {
            StoreApp.MainPage.SelenuimButton.Click();

            Assert.AreEqual(StoreApp.MainPage.GetUrl(), "http://www.seleniumframework.com/");
        }
    }
}
