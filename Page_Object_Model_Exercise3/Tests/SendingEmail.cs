﻿using NUnit.Framework;

namespace Page_Object_Model_Exercise3.Tests
{
    [TestFixture]
    public class SendingEmail : Base
    {
        [SetUp]
        public new void Setup()
        {
            StoreApp.SearchPage.Search();
            StoreApp.SearchPage.WaitForResults();
            StoreApp.SearchPage.SelectProduct();
            StoreApp.ProductPage.WaitForCartForm();
            StoreApp.ProductPage.LoadSendFriendModal();
            StoreApp.ProductPage.WaitForEmailForm();
        }

        [Test]
        public void GivenValidInput_WhenSendingAnEmail_ThenDisplaySuccessModal()
        {
            StoreApp.ProductPage.FriendName.SendKeys("KG");
            StoreApp.ProductPage.FriendEmail.SendKeys("kg@gmail.com");
            StoreApp.ProductPage.SendEmail();
            StoreApp.ProductPage.WaitForEmailSentModal();

            Assert.IsTrue(StoreApp.ProductPage.EmailSentModal.Displayed);
        }

        [Test]
        public void GivenEmptyFields_WhenSendingAnEmail_ThenDisplayValidationMessage()
        {
            StoreApp.ProductPage.SendEmail();
            StoreApp.ProductPage.WaitForErrorMessage();

            Assert.IsTrue(StoreApp.ProductPage.SendEmailError.Displayed);
        }
    }
}
