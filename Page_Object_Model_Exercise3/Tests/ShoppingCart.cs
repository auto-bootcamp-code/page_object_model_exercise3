﻿using NUnit.Framework;

namespace Page_Object_Model_Exercise3.Tests
{
    [TestFixture]
    public class ShoppingCart : Base
    {
        [SetUp]
        public new void Setup()
        {
            StoreApp.SearchPage.Search();
            StoreApp.SearchPage.WaitForResults();
            StoreApp.SearchPage.SelectProduct();
            StoreApp.ProductPage.WaitForCartForm();
            StoreApp.ProductPage.AddTocart();
            StoreApp.ProductPage.WaitForCartModal();
        }

        [Test]
        public void GivenAnExistingItemName_WhenAddingToCart_ThenDisplayCartModal()
        {
            Assert.IsTrue(StoreApp.ProductPage.CartModal.Displayed);
        }

        [Test]
        public void GivenTwoItems_WhenAddingToCart_ThenDisplayTotal()
        {
            StoreApp.ProductPage.CloseCartModal.Click();
            var price = StoreApp.ProductPage.ProductPrice.Text;
            StoreApp.ProductPage.AddTocart();
            StoreApp.ProductPage.WaitForCartModal();

            Assert.AreEqual(StoreApp.ProductPage.GetTotal(price),StoreApp.ProductPage.ProductTotal.Text);
        }

        [Test]
        public void GivenAnExistingCartItem_WhenRemovingFromCart_ThenDisplayAlert()
        {
            StoreApp.ProductPage.CheckoutButton.Click();
            StoreApp.ShoppingCartPage.WaitForShoppingCartPage();
            StoreApp.ShoppingCartPage.CartDeleteButton.Click();
            StoreApp.ShoppingCartPage.WaitForCartAlert();

            Assert.IsTrue(StoreApp.ShoppingCartPage.CartEmptyAlert.Displayed);
        }
    }
}
