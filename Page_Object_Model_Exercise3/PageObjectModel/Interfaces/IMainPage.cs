﻿using OpenQA.Selenium;

namespace Page_Object_Model_Exercise3.PageObjectModel
{
    public interface IMainPage
    {
        IWebElement CenterColumn { get; }
        IWebElement SearchButton { get; }
        IWebElement SearchInput { get; }
        IWebElement SelenuimButton { get; }

        string GetUrl();
    }
}
