﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Page_Object_Model_Exercise3.PageObjectModel
{
    public interface IBasePage
    {
        IWebDriver Driver { get; set; }
        IWebElement PageRoot { get; }
        WebDriverWait Wait { get; set; }
    }
}