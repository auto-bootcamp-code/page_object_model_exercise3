﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace Page_Object_Model_Exercise3.PageObjectModel
{
    public interface ISearchPage
    {
        IList<IWebElement> SearchedItemNames { get; }

        void WaitForResults();
        void Search();
        void SelectProduct();
    }
}