﻿using OpenQA.Selenium;

namespace Page_Object_Model_Exercise3.PageObjectModel
{
    public interface IProductPage
    {
        IWebElement CartButton { get; }
        IWebElement CartForm { get; }
        IWebElement CartModal { get; }
        IWebElement CloseCartModal { get; }
        IWebElement CheckoutButton { get; }
        IWebElement EmailSentModal { get; }
        IWebElement FriendEmail { get; }
        IWebElement FriendName { get; }
        IWebElement SendEmailButton { get; }
        IWebElement SendEmailError { get; }
        IWebElement SendFriendButton { get; }
        IWebElement ProductPrice { get; }
        IWebElement ProductQuantity { get; }
        IWebElement ProductTotal { get; }

        void WaitForCartForm();
        void WaitForCartModal();
        void WaitForEmailForm();
        void WaitForEmailSentModal();
        void WaitForErrorMessage();
        void LoadSendFriendModal();
        void AddTocart();
        void SendEmail();
        string GetTotal(string productprice);

    }
}