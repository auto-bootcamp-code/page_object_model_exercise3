﻿using OpenQA.Selenium;

namespace Page_Object_Model_Exercise3.PageObjectModel
{
    public class ProductPage : IProductPage
    {
        private IBasePage _basePage;
        private IMainPage _mainPage;

        public ProductPage(IBasePage basePage, IMainPage mainPage)
        {
            _basePage = basePage;
            _mainPage = mainPage;
        }

        private IWebElement productPage => _basePage.Driver.FindElement(By.Id("product"));
        private IWebElement emailForm => productPage.FindElement(By.Id("send_friend_form_content"));
        public IWebElement CartForm => _mainPage.CenterColumn.FindElement(By.CssSelector("div.box-cart-bottom"));
        public IWebElement CartButton => CartForm.FindElement(By.Name("Submit"));
        public IWebElement CartModal => productPage.FindElement(By.Id("layer_cart"));
        public IWebElement CheckoutButton => CartModal.FindElement(By.CssSelector("a"));
        public IWebElement CloseCartModal => CartModal.FindElement(By.CssSelector("span"));
        public IWebElement SendFriendButton => productPage.FindElement(By.Id("send_friend_button"));
        public IWebElement FriendName => emailForm.FindElement(By.Id("friend_name"));
        public IWebElement FriendEmail => emailForm.FindElement(By.Id("friend_email"));
        public IWebElement SendEmailButton => productPage.FindElement(By.Id("sendEmail"));
        public IWebElement SendEmailError => productPage.FindElement(By.Id("send_friend_form_error"));
        public IWebElement EmailSentModal => productPage.FindElement(By.CssSelector("div.fancybox-wrap  p"));
        public IWebElement ProductPrice => productPage.FindElement(By.Id("our_price_display"));
        public IWebElement ProductQuantity => productPage.FindElement(By.Id("layer_cart_product_quantity"));
        public IWebElement ProductTotal => productPage.FindElement(By.Id("layer_cart_product_price"));

        public void WaitForCartForm()
        {
            _basePage.Wait.Until(driver => CartForm.Displayed);
        }
        public void SendEmail()
        {
            SendEmailButton.Click();
        }
        public void LoadSendFriendModal()
        {
            SendFriendButton.Click();
        }
        public void AddTocart()
        {
            CartButton.Click();
        }

        public void WaitForCartModal()
        {
            _basePage.Wait.Until(driver => CartModal.Displayed);
        }

        public void WaitForEmailForm()
        {
            _basePage.Wait.Until(driver => emailForm.Displayed);
        }

        public void WaitForEmailSentModal()
        {
            _basePage.Wait.Until(driver => EmailSentModal.Displayed);
        }

        public string GetTotal(string productprice)
        {
            productprice = productprice.Split('$')[1];
            var price = double.Parse(productprice.Replace(".",","));
            var quantity = double.Parse(ProductQuantity.Text);
            var total = string.Concat("$",(price * quantity)).Replace(",", ".");
            return total;
        }
        public void WaitForErrorMessage()
        {
            _basePage.Wait.Until(driver => SendEmailError.Displayed);
        }
    }
}
