﻿using OpenQA.Selenium;

namespace Page_Object_Model_Exercise3.PageObjectModel
{
    public class MainPage : IMainPage
    {
        private IBasePage _basePage;

        public MainPage(IBasePage basePage)
        {
            _basePage = basePage;
        }

        private IWebElement searchBox => _basePage.PageRoot.FindElement(By.Id("searchbox"));
        public IWebElement SearchInput => searchBox.FindElement(By.Name("search_query"));
        public IWebElement SearchButton => searchBox.FindElement(By.Name("submit_search"));
        public IWebElement CenterColumn => _basePage.PageRoot.FindElement(By.Id("center_column"));
        public IWebElement SelenuimButton => _basePage.PageRoot.FindElement(By.CssSelector("#cmsinfo_block a"));

        public string GetUrl()
        {
           return _basePage.Driver.Url;
        }
    }
}
