﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace Page_Object_Model_Exercise3.PageObjectModel
{
    public class SearchPage : ISearchPage
    {
        private IBasePage _basePage;
        private IMainPage _mainPage;

        public SearchPage(IBasePage basePage, IMainPage mainPage)
        {
            _basePage = basePage;
            _mainPage = mainPage;
        }

        public IList<IWebElement> SearchedItemNames => _mainPage.CenterColumn.FindElements(By.CssSelector("a.product-name"));

        public void WaitForResults()
        {
            _basePage.Wait.Until(driver => SearchedItemNames.Count > 0);
        }

        public void SelectProduct()
        {
           SearchedItemNames[0].Click();
        }

        public void Search()
        {
           _mainPage.SearchInput.SendKeys("Dress");
            _mainPage.SearchButton.Click();
        }
    }
}
